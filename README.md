Mezzanine Mailchimp
==================

Email subscribe forms of mezzanine to a MailChimp list

Features
--------

 - Email subscribe forms of mezzanine to a MailChimp list


How to Use
----------

 1. Get and install the package:

        pip install mezzanine-mailchimp

    or

        git clone git@bitbucket.org:naritasltda/mezzanine-mailchimp.git

        cd mezzanine-mailchimp

        python setup.py install

    Mezzanine 1.3 or higher is required.

 2. Install the app in your Mezzanine project by adding
    `mezzanine_mailchimp` to the list of `INSTALLED_APPS` in your
    project's `settings.py`.

 3. Configure Api key and List id in:

    localhost:8000/admin/conf/setting/

    Fields: "Mailchimp Api Key" and "Mailchimp list id".




License
-------

Licence: BSD. See included `LICENSE` file.

Note that this license applies to this repository only.